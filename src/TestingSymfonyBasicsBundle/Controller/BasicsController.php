<?php
/**
 * Created by PhpStorm.
 * User: Bert
 * Date: 1/12/2016
 * Time: 13:36
 */

namespace TestingSymfonyBasicsBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BasicsController extends Controller
{
    /**
     * @Route("/helloworld", name="testing_symfony_basics_hello_world")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function helloAction()
    {
        return $this->render('TestingSymfonyBasicsBundle:Basics:hello.html.twig');
    }

}