<?php
/**
 * Created by PhpStorm.
 * User: Bert
 * Date: 1/12/2016
 * Time: 13:11
 */

namespace TestingSymfonyBasicsBundle\Libs;


class Greeter
{
    public function display($name)
    {
        return $name.", you are the coolest person I've ever met!";
    }
}