<?php

namespace TestingSymfonyBasicsBundle\Tests\Libs;

use TestingSymfonyBasicsBundle\Libs\Greeter;
/**
 * Created by PhpStorm.
 * User: Bert
 * Date: 1/12/2016
 * Time: 13:14
 */
class GreeterTest extends \PHPUnit_Framework_TestCase
{
    public function testDisplay()
    {
        $greeting = new Greeter();
        $result = $greeting->display("John");
        $expected = "John, you are the coolest person I've ever met!";
        $this->assertEquals($expected, $result);
    }
}